﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace MillionenshowClient
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Client c;
        private bool answered;
        private Button lastColoredButton;
        public MainWindow()
        {
            answered = false;
            InitializeComponent();
            c = new Client("127.0.0.1");
            XmlDocument doc = c.WaitForServerReply();
            ReactToTheServer(doc);
        }

        private void ReactToTheServer(XmlDocument doc)
        {
            XmlNode rootNode = doc.SelectSingleNode("RESPONSE");
            XmlNode gameIDNode = rootNode.SelectSingleNode("GAMEID");
            XmlNode levelNode = rootNode.SelectSingleNode("LEVEL");
            XmlNode questionNode = rootNode.SelectSingleNode("QUESTION");
            XmlNode answer1Node = rootNode.SelectSingleNode("ANSWER1");
            XmlNode answer2Node = rootNode.SelectSingleNode("ANSWER2");
            XmlNode answer3Node = rootNode.SelectSingleNode("ANSWER3");
            XmlNode answer4Node = rootNode.SelectSingleNode("ANSWER4");

            answerA.Content = "A: "+answer1Node?.InnerText;
            answerB.Content = "B: " + answer2Node?.InnerText;
            answerC.Content = "C: " + answer3Node?.InnerText;
            answerD.Content = "D: " + answer4Node?.InnerText;

            gameID.Text = gameIDNode?.InnerText;
            c.GameID = Convert.ToInt32(gameID.Text);
            levelID.Text = levelNode?.InnerText;
            question.Text = questionNode?.InnerText;
        }

        private void answerLocked(object sender, RoutedEventArgs e)
        {
            if (!answered)
            {
                c.ID = "3";
                Button b = (Button)sender;
                String s = b.Content.ToString();
                switch (s[0])
                {
                    case 'A':
                        c.SendAnswerCheck("1");
                        FillResult(c.WaitForServerReply(), b);
                        break;
                    case 'B':
                        c.SendAnswerCheck("2");
                        FillResult(c.WaitForServerReply(), b);
                        break;
                    case 'C':
                        c.SendAnswerCheck("3");
                        FillResult(c.WaitForServerReply(), b);
                        break;
                    case 'D':
                        c.SendAnswerCheck("4");
                        FillResult(c.WaitForServerReply(), b);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                nextQuestion_Click(sender, e);
            }
            
        }

        private void FillResult(XmlDocument doc, Button b)
        {
            XmlNode rootNode = doc.SelectSingleNode("RESPONSE");
            XmlNode correctnessNode = rootNode.SelectSingleNode("CORRECT");
            if (correctnessNode?.InnerText == "true")
            {
                b.Background = Brushes.Green;
            }
            else
            {
                b.Background = Brushes.Red;
                SendEnd();
                b.Background = Brushes.LightGray;
            }
            lastColoredButton = b;
            answered = true;
        }

        private void SendEnd()
        {
            c.ID = "4";
            c.KillConnection();
            XmlDocument doc = c.WaitForServerReply();
            XmlNode rootNode = doc.SelectSingleNode("RESPONSE");
            XmlNode beginNode = rootNode.SelectSingleNode("BEGIN");
            XmlNode endNode = rootNode.SelectSingleNode("END");
            MessageBox.Show("You lost!\nYour time: \n"
                + beginNode.InnerText +
                " - "
                + endNode.InnerText
                );
            SetDefaultValues();
        }

        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            c = new Client(c.IP);
            XmlDocument doc = c.WaitForServerReply();
            ReactToTheServer(doc);
            lastColoredButton.Background = Brushes.LightGray;
        }

        private void EndGame_Click(object sender, RoutedEventArgs e)
        {
            c.ID = "4";
            c.KillConnection();
            XmlDocument doc = c.WaitForServerReply();
            XmlNode rootNode = doc.SelectSingleNode("RESPONSE");
            XmlNode beginNode = rootNode.SelectSingleNode("BEGIN");
            XmlNode endNode = rootNode.SelectSingleNode("END");
            MessageBox.Show("Your time: \n"
                + beginNode.InnerText + 
                " - " 
                + endNode.InnerText
                );
            SetDefaultValues();
            lastColoredButton.Background = Brushes.LightGray;
        }

        private void SetDefaultValues()
        {
            answerA.Content = "A: ";
            answerB.Content = "B: ";
            answerC.Content = "C: ";
            answerD.Content = "D: ";

            gameID.Text = "?";
            c.GameID = -1;
            levelID.Text = "?";
            question.Text = "The question is, what is the question \nPress New Game to start";
        }

        private void nextQuestion_Click(object sender, RoutedEventArgs e)
        {
            if (answered)
            {
                c.ID = "2";
                c.SendRequestForNewQuestion();
                ReactToTheServer(c.WaitForServerReply());
                lastColoredButton.Background = Brushes.LightGray;
                answered = false;
            }
            else
            {
                MessageBox.Show("please answer the Question!");
            }
        }
    }
}
