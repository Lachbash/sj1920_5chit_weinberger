﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace MillionenshowClient
{
    class Client
    {
        private TcpClient client;
        private NetworkStream nwStream;
        private XmlDocument doc;
        public String IP { get;}
        public int GameID { get; set; }
        public String ID { get; set; }

        public Client(string IP)
        {
            GameID = -1;
            this.IP = IP;
            ID = "1";
            DeclareBeginningMessage();
            try
            {
                client = new TcpClient(IP, 3333);
                nwStream = client.GetStream();
                SendToServer();
            }
            catch (Exception)
            {
                MessageBox.Show("Couldn't connect to Server");
                System.Environment.Exit(0);
            }
        }

        public XmlDocument WaitForServerReply()
        {
            byte[] bytesToRead = new byte[client.ReceiveBufferSize];
            int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize); //reading the reply from the server
            return ReadMessage(Encoding.Default.GetString(bytesToRead, 0, bytesRead));
        }

        public XmlDocument ReadMessage(string recievedMessage)
        {
            try
            {
                doc.LoadXml(recievedMessage); //loads the Data into the doc
                return doc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(recievedMessage);
                MessageBox.Show(ex.Message);
                MessageBox.Show("Can't Connect to Server");
                throw;
            }
        }

        public void DeclareBeginningMessage()
        {
            //generating the first Message
            doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); //definition of the xml
            doc.AppendChild(docNode);

            //building the xml
            XmlNode rootNode = doc.CreateElement("REQUEST");
            XmlNode idNode = doc.CreateElement("ID");
            idNode.InnerText = ID;
            XmlNode gameID = doc.CreateElement("GAMEID");
            XmlNode answerID = doc.CreateElement("ANSWERID");

            //append the nodes
            doc.AppendChild(rootNode);
            rootNode.AppendChild(idNode);
            rootNode.AppendChild(gameID);
            rootNode.AppendChild(answerID);
        }

        public string SendToServer()
        {
            string message = doc?.OuterXml;
            byte[] bytesToSend = ASCIIEncoding.Default.GetBytes(message); //create the message for the server

            //Console.WriteLine("Client-Message: " + message); DEBUG INFO
            nwStream.Write(bytesToSend, 0, bytesToSend.Length); //sending the message to the server

            
            return message;
        }

        public void SendAnswerCheck(String index)
        {
            RenewConnection();

            doc = new XmlDocument();

            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); //definition of the xml
            doc.AppendChild(docNode);

            //building the xml
            XmlNode rootNode = doc.CreateElement("REQUEST");
            XmlNode idNode = doc.CreateElement("ID");
            idNode.InnerText = ID;
            XmlNode gameID = doc.CreateElement("GAMEID");
            gameID.InnerText = GameID.ToString();
            XmlNode answerID = doc.CreateElement("ANSWERID");
            answerID.InnerText = index;

            //append the nodes
            doc.AppendChild(rootNode);
            rootNode.AppendChild(idNode);
            rootNode.AppendChild(gameID);
            rootNode.AppendChild(answerID);

            SendToServer();
        }

        public void SendRequestForNewQuestion()
        {
            RenewConnection();

            doc = new XmlDocument();

            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); //definition of the xml
            doc.AppendChild(docNode);

            //building the xml
            XmlNode rootNode = doc.CreateElement("REQUEST");
            XmlNode idNode = doc.CreateElement("ID");
            idNode.InnerText = ID;
            XmlNode gameID = doc.CreateElement("GAMEID");
            gameID.InnerText = GameID.ToString();
            XmlNode answerID = doc.CreateElement("ANSWERID");

            //append the nodes
            doc.AppendChild(rootNode);
            rootNode.AppendChild(idNode);
            rootNode.AppendChild(gameID);
            rootNode.AppendChild(answerID);

            SendToServer();
        }

        private void RenewConnection()
        {
            nwStream.Close();
            nwStream.Flush();

            try
            {
                client = new TcpClient(IP, 3333);
                nwStream = client.GetStream();
            }
            catch (Exception)
            {
                MessageBox.Show("Couldn't connect to Server");
            }
        }

        public void KillConnection()
        {
            ID = "4";
            RenewConnection();
            doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); //definition of the xml
            doc.AppendChild(docNode);
            //building the xml
            XmlNode rootNode = doc.CreateElement("REQUEST");
            XmlNode idNode = doc.CreateElement("ID");
            idNode.InnerText = ID;
            XmlNode gameID = doc.CreateElement("GAMEID");
            gameID.InnerText = GameID.ToString();
            XmlNode answerID = doc.CreateElement("ANSWERID");

            //append the nodes
            doc.AppendChild(rootNode);
            rootNode.AppendChild(idNode);
            rootNode.AppendChild(gameID);
            rootNode.AppendChild(answerID);

            SendToServer();
        }
    }
}
