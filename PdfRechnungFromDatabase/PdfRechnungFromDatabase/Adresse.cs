﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PdfRechnungFromDatabase
{
    public class Adresse
    {

        public int ID { get; set; }

        public String Strasse { get; set; }

        public int Hausnummer { get; set; }

        public String PLZ { get; set; }

        public String Ort { get; set; }

        public override string ToString()
        {
            return Strasse + " " + Hausnummer + " " + PLZ + " " + Ort;
        }
    }
}