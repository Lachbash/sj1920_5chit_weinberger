﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PdfRechnungFromDatabase
{
    public class ArtikelInRechnung
    {
        public int ID { get; set; }

        public int Rechnungs_ID { get; set; }

        public Rechnung Rechnung { get; set; }

        public int Artikel_ID { get; set; }

        public Artikel Artikel { get; set; }

    }
}