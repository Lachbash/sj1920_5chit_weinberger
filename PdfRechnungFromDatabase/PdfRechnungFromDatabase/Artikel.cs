﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PdfRechnungFromDatabase
{
    public class Artikel
    {

        public int ID { get; set; }

        public String Bezeichnung { get; set; }

        public double Preis { get; set; }

        public int EinheitID { get; set; }

        public Einheit Einheit { get; set; }

        public int RabattID { get; set; }

        public Rabatt Rabatt { get; set; }
    }
}