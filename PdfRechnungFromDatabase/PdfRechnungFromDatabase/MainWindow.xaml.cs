﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PdfRechnungFromDatabase
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataGetter dataGetter;

        PdfMaker pdfMaker;

        int selectedID;

        public MainWindow()
        {
            InitializeComponent();
            dataGetter = new DataGetter();
            dataGetter.getDataFromDatabase();
            pdfMaker = new PdfMaker(dataGetter);
            showAvaiableIDs();
        }

        private void showAvaiableIDs()
        {
            for (int i = 0; i < dataGetter.RechnungListe.Count; i++)
            {
                Rechnung r = (Rechnung)dataGetter.RechnungListe[i];
                Button b = new Button();
                b.Content = r.ID;
                b.Click += IDSelected;
                stackPanel.Children.Add(b);
            }         
        }

        private void MainWindowIsLoaded(object sender, RoutedEventArgs e)
        {

        }

        private void IDSelected(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            selectedID = Convert.ToInt32(b.Content);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (selectedID != 0)
            {
                pdfMaker.makePdf(selectedID);
            }
            else
            {
                MessageBox.Show("ID not shown");
            }
        }
    }
}
