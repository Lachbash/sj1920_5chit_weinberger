﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PdfRechnungFromDatabase
{
    public class Name
    {
        public int ID { get; set; }

        public String Vorname { get; set; }

        public String Nachname { get; set; }

        public override string ToString()
        {
            return Nachname + " " + Vorname;
        }
    }
}