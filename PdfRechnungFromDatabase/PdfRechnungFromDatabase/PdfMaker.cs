﻿using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Syncfusion.Pdf.Interactive;
using Syncfusion.Pdf.Tables;
using System.Data;

namespace PdfRechnungFromDatabase
{
    public class PdfMaker
    {
        float yCount;

        PdfPage currentPage;

        double sum;

        private DataGetter dataGetter;

        public PdfMaker(DataGetter dataGetter)
        {
            this.dataGetter = dataGetter;
        }

        public void makePdf(int rechnungsID)
        {

            yCount = 0;

            float center;

            var rechnung = dataGetter.getOneRechnungAndOtherDataForBill(rechnungsID);

            PdfFont fontHeading = new PdfStandardFont(PdfFontFamily.Helvetica, 18f);
            PdfFont fontText = new PdfStandardFont(PdfFontFamily.Helvetica, 11f);
            PdfFont fontSubHeading = new PdfStandardFont(PdfFontFamily.Helvetica, 14f);


            PdfBrush brush = new PdfSolidBrush(Color.Black);

            PdfDocument doc = new PdfDocument();

            doc.Pages.PageAdded += newPage;

            PdfSection sectionFirstPage = doc.Sections.Add();
            PdfPage firstPage = sectionFirstPage.Pages.Add();

            PdfSection sectionLastPage = doc.Sections.Add();



            RectangleF boundsHeader = new RectangleF(0, 0, doc.Pages[0].GetClientSize().Width, 300);

            RectangleF boundsHeaderOther = new RectangleF(0, 0, doc.Pages[0].GetClientSize().Width, 150);

            RectangleF boundsFooter = new RectangleF(0, 0, doc.Pages[0].GetClientSize().Width, 50);

            center = doc.Pages[0].GetClientSize().Width / 2 - 50;

            //Header start Header
            PdfPageTemplateElement header = new PdfPageTemplateElement(boundsHeader);
            header.Graphics.DrawString(rechnung.rechnung.Firma.Bezeichnung +" - Daniel Weinberger", fontHeading, brush, new PointF(center, yCount));
            yCount += fontHeading.Height;
            header.Graphics.DrawString(rechnung.rechnung.Firma.Adresse.ToString(), fontText, brush, new PointF(center, yCount));
            yCount += fontText.Height;


            PdfPen pen = new PdfPen(PdfBrushes.Black, 1f);
            yCount += 10;

            PointF point1 = new PointF(0, yCount);
            PointF point2 = new PointF(doc.Pages[0].GetClientSize().Width, yCount);
            yCount += 2;
            header.Graphics.DrawLine(pen, point1, point2);

            point1 = new PointF(0, yCount);
            point2 = new PointF(doc.Pages[0].GetClientSize().Width, yCount);
            yCount += 2;
            header.Graphics.DrawLine(pen, point1, point2);

            yCount += 40;

            header.Graphics.DrawString(rechnung.rechnung.Kunde.Name.ToString(), fontText, brush, new PointF(center / 4, yCount));
            yCount += fontText.Height;
            yCount += 20;
            header.Graphics.DrawString(rechnung.rechnung.Kunde.Adresse.ToString(), fontText, brush, new PointF(center / 4, yCount));
            yCount += fontText.Height;

            yCount += 50;
            header.Graphics.DrawString("Rechnung - " + rechnung.rechnung.Ausstellungsdatum.Year + "/" + rechnung.rechnung.ID, fontSubHeading, brush, new PointF(center / 8, yCount));
            header.Graphics.DrawString(rechnung.rechnung.Ausstellungsdatum.ToShortDateString(), fontText, brush, new PointF(center * 2 - center / 16, yCount));
            yCount += fontSubHeading.Height;
            header.Graphics.DrawString(rechnung.rechnung.Projekttext, fontText, brush, new PointF(center / 8, yCount));
            header.Graphics.DrawString(rechnung.rechnung.Sachbearbeiter.Name.ToString(), fontText, brush, new PointF(center * 2 - center / 16, yCount));
            yCount += 10 + fontText.Height;

            firstPage.Graphics.DrawString(rechnung.rechnung.BillDescription, fontText, brush, new PointF(center / 8, yCount));
            yCount += 10 + fontText.Height;

            //Header First Page End 

            //Header Other Pages Start

            float yOtherPages = 0;

            PdfPageTemplateElement headerOtherPages = new PdfPageTemplateElement(boundsHeaderOther);

            headerOtherPages.Graphics.DrawString("Rechnung - " + rechnung.rechnung.Ausstellungsdatum.Year + "/" + rechnung.rechnung.ID, fontSubHeading, brush, new PointF(center / 8, yOtherPages));
            headerOtherPages.Graphics.DrawString(rechnung.rechnung.Ausstellungsdatum.ToShortDateString(), fontText, brush, new PointF(center * 2 - center / 16, yOtherPages));
            yOtherPages += fontSubHeading.Height;
            headerOtherPages.Graphics.DrawString(rechnung.rechnung.Projekttext, fontText, brush, new PointF(center / 8, yOtherPages));
            headerOtherPages.Graphics.DrawString(rechnung.rechnung.Sachbearbeiter.Name.ToString(), fontText, brush, new PointF(center * 2 - center / 16, yOtherPages));
            yOtherPages += 10 + fontText.Height;

            //Header Other Pages End

            sum = 0;
            drawTable(center, rechnung, fontText, doc, firstPage, sectionLastPage, yCount, 0, 0);

            currentPage = doc.Pages[doc.Pages.Count-1];

            currentPage.Graphics.DrawString("Zahlungskonditionen", fontText, brush, new PointF(center / 8, yCount));
            currentPage.Graphics.DrawString("Summe-Netto", fontText, brush, new PointF(center, yCount));
            currentPage.Graphics.DrawString(sum + "", fontText, brush, new PointF(center + center / 2, yCount));
            yCount += fontText.Height;
            currentPage.Graphics.DrawString(rechnung.rechnung.Zahlungskonditionen.ToString(), fontText, brush, new PointF(center / 8, yCount));
            currentPage.Graphics.DrawString(rechnung.rechnung.Zahlungskonditionen.Skonto +"", fontText, brush, new PointF(center, yCount));
            currentPage.Graphics.DrawString(rechnung.rechnung.Zahlungskonditionen.Skonto+"", fontText, brush, new PointF(center + center / 2, yCount));
            yCount += fontText.Height;
            currentPage.Graphics.DrawString("abzügl. Summe", fontText, brush, new PointF(center, yCount));
            currentPage.Graphics.DrawString( Convert.ToDecimal(sum)*((decimal)rechnung.rechnung.Zahlungskonditionen.Skonto/100)+"", fontText, brush, new PointF(center + center / 2, yCount));
            yCount += fontText.Height;
            currentPage.Graphics.DrawString("Ust. 10% von", fontText, brush, new PointF(center, yCount));
            currentPage.Graphics.DrawString(sum * 0.1 + "", fontText, brush, new PointF(center + center / 2, yCount));
            yCount += fontText.Height;
            currentPage.Graphics.DrawString("Ust. 20% von", fontText, brush, new PointF(center, yCount));
            currentPage.Graphics.DrawString(sum * 0.2 + "", fontText, brush, new PointF(center + center / 2, yCount));
            yCount += fontText.Height+10;
            currentPage.Graphics.DrawString("Endbetrag", fontText, brush, new PointF(center, yCount));
            currentPage.Graphics.DrawString(Convert.ToDecimal(sum) - Convert.ToDecimal(sum) * ((decimal)rechnung.rechnung.Zahlungskonditionen.Skonto / 100) - Convert.ToDecimal(sum * 0.1) + "", fontText, brush, new PointF(center + center / 2, yCount));
            yCount += fontText.Height;


            //Footer
            PdfPageTemplateElement footer = new PdfPageTemplateElement(boundsFooter);
            PdfPageNumberField pageNumber = new PdfPageNumberField(fontText, brush);
            PdfPageCountField count = new PdfPageCountField(fontText, brush);
            PdfCompositeField compositeField = new PdfCompositeField(fontText, brush, "{0} of {1}", pageNumber, count);
            compositeField.Bounds = footer.Bounds;
            compositeField.Draw(footer.Graphics, new PointF(470, 40));

            footer.Graphics.DrawString(rechnung.rechnung.FooterText, fontText, brush, new PointF(boundsFooter.Width/10, (boundsFooter.Height / 2) - (fontText.Height / 2)));

            //Footer Other Pages
            PdfPageTemplateElement footerOtherPages = new PdfPageTemplateElement(boundsFooter);
            PdfCompositeField compositeField2 = new PdfCompositeField(fontText, brush, "{0} of {1}", pageNumber, count);
            compositeField2.Bounds = footer.Bounds;
            compositeField2.Draw(footerOtherPages.Graphics, new PointF(470, 40));

            footerOtherPages.Graphics.DrawString("Wir danken für Ihr Vertrauen und hoffen, Sie bald wieder als Kunden begrüßen zu dürfen!", fontText, brush, new PointF(boundsFooter.Width / 10, (boundsFooter.Height / 2) - 4*(fontText.Height / 2)));

            footerOtherPages.Graphics.DrawString("Mit freundlichen Grüßen,", fontText, brush, new PointF(boundsFooter.Width / 10, (boundsFooter.Height / 2) - (fontText.Height / 2)));

            footerOtherPages.Graphics.DrawString(rechnung.rechnung.Firma.Bezeichnung +"!", fontText, brush, new PointF(boundsFooter.Width / 10, (boundsFooter.Height / 2) + (fontText.Height / 2)));

            sectionFirstPage.Template.Bottom = footer;
            sectionFirstPage.Template.Top = header;

            sectionLastPage.Template.Bottom = footerOtherPages;
            sectionLastPage.Template.Top = headerOtherPages;

            doc.Save("Rechnung.pdf");


        }

        private void drawTable(float center, (Rechnung rechnung, List<(Artikel artikel, int menge)> artikel) rechnung, PdfFont fontText, PdfDocument doc, PdfPage firstPage, PdfSection sectionLastPage, float yCount,int startingPoint, int docCounter)
        {
            PdfLightTable pdfLightTable = new PdfLightTable();
            DataTable table = new DataTable();

            table.Columns.Add("pos");
            table.Columns.Add("Text");
            table.Columns.Add("Menge");
            table.Columns.Add("Eh");
            table.Columns.Add("Preis/Eh");
            table.Columns.Add("Rab");
            table.Columns.Add("Netto");


            float yCountTemp = yCount;

            for (int i = startingPoint; i < 150; i++)
            {
                var a = rechnung.artikel[1];
                table.Rows.Add(new string[] {(i+1).ToString(),
                    a.artikel.Bezeichnung,a.menge.ToString(),
                    a.artikel.Einheit.Bezeichnung,
                    a.artikel.Preis.ToString(),
                    "-"+ a.artikel.Rabatt.Wert +"%",
                    (((100-a.artikel.Rabatt.Wert )/100)*a.artikel.Preis).ToString()
                });

                sum += a.artikel.Preis * a.menge;
                yCountTemp += fontText.Height + 1;
                if (yCountTemp +50 >= doc.Pages[docCounter].GetClientSize().Height)
                {
                    pdfLightTable.DataSource = table;
                    pdfLightTable.Style.ShowHeader = true;
                    pdfLightTable.Draw(firstPage, new PointF(center / 8, yCount));
                    PdfPage page = sectionLastPage.Pages.Add();
                    drawTable(center, rechnung, fontText, doc, page, sectionLastPage, 100, i +1, docCounter);
                    break;
                }
            }
            yCount = yCountTemp;

        }

        private void newPage(object sender, PageAddedEventArgs args)
        {
            //throw new NotImplementedException();
        }

        public void preShowPdf()
        {

        }
    }
}