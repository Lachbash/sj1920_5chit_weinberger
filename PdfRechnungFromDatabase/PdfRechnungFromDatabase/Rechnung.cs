﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PdfRechnungFromDatabase
{
    public class Rechnung
    {
        public int ID { get; set; }

        public DateTime Ausstellungsdatum { get; set; }

        public String Projekttext { get; set; }

        public String BillDescription { get; set; }

        public String Description { get; set; }

        public String FooterText{ get; set; }

        public int KundeID { get; set; }

        public Kunde Kunde{ get; set; }

        public int FirmaID { get; set; }

        public Firma Firma{ get; set; }

        public int SachbearbeiterID { get; set; }

        public Sachbearbeiter Sachbearbeiter { get; set; }

        public int ZahlungskonditionenID { get; set; }

        public Zahlungskonditionen Zahlungskonditionen { get; set; }

        public int RabattID { get; set; }

        public Rabatt Rabatt{ get; set; }
    }
}