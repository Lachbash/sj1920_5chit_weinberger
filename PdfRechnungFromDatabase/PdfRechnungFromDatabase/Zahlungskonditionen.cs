﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PdfRechnungFromDatabase
{
    public class Zahlungskonditionen
    {
        public int ID { get; set; }

        public int UntergrenzeTage { get; set; }

        public int Skonto { get; set; }

        public int ObergrenzeTage { get; set; }

        public override string ToString()
        {
            return UntergrenzeTage + " Tage " + Skonto + "% Skonto, " + ObergrenzeTage + " Tage Netto"; 
        }
    }
}