﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PdfRechnungFromDatabase
{
    public class Firma
    {

        public int ID { get; set; }

        public int AdresseID { get; set; }

        public Adresse Adresse { get; set; }

        public String Bezeichnung { get; set; }
    }
}