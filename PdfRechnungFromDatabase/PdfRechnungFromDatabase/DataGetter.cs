﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Windows;
using MySql.Data.MySqlClient;

namespace PdfRechnungFromDatabase
{
    public class DataGetter
    {
        public String ConnectionString => "Server=localhost;Database=rechnungen;Uid=root;Pwd=;";

        public DataGetter()
        {

        }

        public void getDataFromDatabase()
        {
            using (MySqlConnection sqlConnection = new MySqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                renewArtikelData(sqlConnection);
                renewAddressData(sqlConnection);
                renewArtikelInRechnungData(sqlConnection);
                renewEinheitData(sqlConnection);
                renewFirmenData(sqlConnection);
                renewKundenData(sqlConnection);
                renewNameData(sqlConnection);
                renewRabattData(sqlConnection);
                renewRechnungData(sqlConnection);
                renewSachbearbeiterData(sqlConnection);
                renewZahlungskonditionenData(sqlConnection);

                fillArtikelIDs();
                fillArtikelInRechnungIDs();
                fillFirmenIDs();
                fillKundenIDs();
                fillRechnungIDs();
                fillSachbearbeiterIDs();

            }
        }

        private void fillSachbearbeiterIDs()
        {
            foreach (Sachbearbeiter s in SachbearbeiterListe)
            {
                IEnumerable<Name> querryResult = from name in NameListe
                                                 where name.ID == s.NameID
                                                 select name;

                try
                {
                    s.Name = querryResult.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " couldn't find fitting Name");
                }

            }
        }

        private void fillRechnungIDs()
        {
            foreach (Rechnung r in RechnungListe)
            {
                IEnumerable<Kunde> querryResult = from kunde in KundenListe
                                                  where kunde.ID == r.KundeID
                                                  select kunde;
                try
                {
                    r.Kunde = querryResult.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " couldn't find fitting Kunde");
                }

                IEnumerable<Firma> querryResult2 = from firma in FirmenListe
                                                   where firma.ID == r.FirmaID
                                                   select firma;
                try
                {
                    r.Firma = querryResult2.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " couldn't find fitting Firma");
                }

                IEnumerable<Firma> querryResult3 = from firma in FirmenListe
                                                   where firma.ID == r.FirmaID
                                                   select firma;
                try
                {
                    r.Firma = querryResult3.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " couldn't find fitting Firma");
                }

                IEnumerable<Sachbearbeiter> querryResult4 = from sachbeabeiter in SachbearbeiterListe
                                                            where sachbeabeiter.ID == r.SachbearbeiterID
                                                            select sachbeabeiter;
                try
                {
                    r.Sachbearbeiter = querryResult4.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " couldn't find fitting Sachbearbeiter");
                }

                IEnumerable<Rabatt> querryResult5 = from rabatt in RabattListe
                                                    where rabatt.ID == r.RabattID
                                                    select rabatt;
                try
                {
                    r.Rabatt = querryResult5.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " couldn't find fitting Rabatt");
                }

                IEnumerable<Zahlungskonditionen> querryResult6 = from zahlungskonditionen in ZahlungskonditionenList
                                                    where zahlungskonditionen.ID == r.ZahlungskonditionenID
                                                    select zahlungskonditionen;
                try
                {
                    r.Zahlungskonditionen = querryResult6.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " couldn't find fitting Zahlungskonditionen");
                }
            }
        }

        private void fillKundenIDs()
        {
            foreach (Kunde k in KundenListe)
            {
                IEnumerable<Name> querryResult = from name in NameListe
                                                 where name.ID == k.NameID
                                                 select name;

                try
                {
                    k.Name = querryResult.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " couldn't find fitting Name");
                }

                IEnumerable<Adresse> querryResult2 = from adresse in AdressenListe
                                                     where adresse.ID == k.AdresseID
                                                     select adresse;

                try
                {
                    k.Adresse = querryResult2.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " couldn't find fitting Adresse");
                }

            }
        }

        private void fillFirmenIDs()
        {
            foreach (Firma f in FirmenListe)
            {
                IEnumerable<Adresse> querryResult = from adresse in AdressenListe
                                                    where adresse.ID == f.AdresseID
                                                    select adresse;

                try
                {
                    f.Adresse = querryResult.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " couldn't find fitting Rechnung");
                }

            }
        }

        private void fillArtikelInRechnungIDs()
        {
            foreach (ArtikelInRechnung a in ArtikelInRechnungListe)
            {
                IEnumerable<Rechnung> querryResult = from rechnung in RechnungListe
                                                     where rechnung.ID == a.Rechnungs_ID
                                                     select rechnung;

                try
                {
                    a.Rechnung = querryResult.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + "couldn't find fitting Rechnung");
                }

                IEnumerable<Artikel> querryResult2 = from artikel in ArtikelList
                                                     where artikel.ID == a.Artikel_ID
                                                     select artikel;

                try
                {
                    a.Artikel = querryResult2.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + "couldn't find fitting Artikel");
                }

            }
        }

        private void fillArtikelIDs()
        {
            foreach (Artikel a in ArtikelList)
            {
                IEnumerable<Einheit> querryResult = from einheit in EinheitListe
                                                    where einheit.ID == a.EinheitID
                                                    select einheit;

                try
                {
                    a.Einheit = querryResult.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + "couldn't find fitting Einheit");
                }

                IEnumerable<Rabatt> querryResult2 = from rabatt in RabattListe
                                                    where rabatt.ID == a.RabattID
                                                    select rabatt;

                try
                {
                    a.Rabatt = querryResult2.First();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + "couldn't find fitting Rabatt");
                }

            }
        }

        private void renewZahlungskonditionenData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from zahlungskonditionen"))
            {
                ZahlungskonditionenList = new List<Zahlungskonditionen>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    ZahlungskonditionenList.Add(new Zahlungskonditionen
                    {
                        ID = (int)reader[0],
                        UntergrenzeTage = (int)reader[1],
                        Skonto = (int)reader[2],
                        ObergrenzeTage = (int)reader[3]
                    });
                }
            }
        }

        private void renewSachbearbeiterData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from sachbearbeiter"))
            {
                SachbearbeiterListe = new List<Sachbearbeiter>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    SachbearbeiterListe.Add(new Sachbearbeiter
                    {
                        ID = (int)reader[0],
                        NameID = (int)reader[1]
                    });
                }
            }
        }

        private void renewRechnungData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from rechnung"))
            {
                RechnungListe = new List<Rechnung>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    RechnungListe.Add(new Rechnung
                    {
                        ID = (int)reader[0],
                        Ausstellungsdatum = (DateTime)reader[1],
                        BillDescription = reader[2].ToString(),
                        Projekttext = reader[3].ToString(),
                        Description = reader[4].ToString(),
                        FooterText = reader[5].ToString(),
                        KundeID = (int)reader[6],
                        FirmaID = (int)reader[7],
                        SachbearbeiterID = (int)reader[8],
                        ZahlungskonditionenID = (int)reader[9],
                        RabattID = (int)reader[10]
                    });
                }
            }
        }

        private void renewRabattData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from rabatt"))
            {
                RabattListe = new List<Rabatt>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    RabattListe.Add(new Rabatt
                    {
                        ID = (int)reader[0],
                        Wert = (int)reader[1]
                    });
                }
            }
        }

        private void renewNameData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from name"))
            {
                NameListe = new List<Name>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    NameListe.Add(new Name
                    {
                        ID = (int)reader[0],
                        Vorname = reader[1].ToString(),
                        Nachname = reader[2].ToString()
                    });
                }
            }
        }

        private void renewKundenData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from kunde"))
            {
                KundenListe = new List<Kunde>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    KundenListe.Add(new Kunde
                    {
                        ID = (int)reader[0],
                        AdresseID = (int)reader[1],
                        NameID = (int)reader[2],
                    });
                }
            }
        }

        private void renewFirmenData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from firma"))
            {
                FirmenListe = new List<Firma>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    FirmenListe.Add(new Firma
                    {
                        ID = (int)reader[0],
                        AdresseID = (int)reader[1],
                        Bezeichnung = reader[2].ToString()
                    });
                }
            }
        }

        private void renewEinheitData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from einheit"))
            {
                EinheitListe = new List<Einheit>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    EinheitListe.Add(new Einheit
                    {
                        ID = (int)reader[0],
                        Bezeichnung = reader[1].ToString()
                    });
                }
            }
        }

        private void renewArtikelInRechnungData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from artikel_in_rechnung"))
            {
                ArtikelInRechnungListe = new List<ArtikelInRechnung>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    ArtikelInRechnungListe.Add(new ArtikelInRechnung
                    {
                        ID = (int)reader[0],
                        Rechnungs_ID = (int)reader[1],
                        Artikel_ID = (int)reader[2],
                    });
                }
            }
        }

        private void renewAddressData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from adresse"))
            {
                AdressenListe = new List<Adresse>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    AdressenListe.Add(new Adresse
                    {
                        ID = (int)reader[0],
                        Strasse = reader[1].ToString(),
                        Hausnummer = (int)reader[2],
                        PLZ = reader[3].ToString(),
                        Ort = reader[4].ToString()
                    });
                }
            }
        }

        private void renewArtikelData(MySqlConnection sqlConnection)
        {
            using (MySqlCommand mySqlCommand = new MySqlCommand("select * from artikel"))
            {
                ArtikelList = new List<Artikel>();
                mySqlCommand.Connection = sqlConnection;

                MySqlDataReader reader = null;
                reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {

                    ArtikelList.Add(new Artikel
                    {
                        ID = (int)reader[0],
                        Bezeichnung = reader[1].ToString(),
                        Preis = Convert.ToDouble(reader[2]),
                        EinheitID = (int)reader[3],
                        RabattID = (int)reader[4]
                    });
                }
            }
        }

        public (Rechnung rechnung, List<(Artikel artikel, int menge)> artikel) getOneRechnungAndOtherDataForBill(int id)
        {
            List<ArtikelInRechnung> artikelInRechnung = ArtikelInRechnungListe.FindAll(a => a.Rechnungs_ID == id);
            List<(Artikel, int)> artikel = new List<(Artikel, int)>();
            List<int> alreadyIn = new List<int>();

            foreach (ArtikelInRechnung a in artikelInRechnung)
            {
                if (!alreadyIn.Contains(a.Artikel_ID))
                {
                    alreadyIn.Add(a.Artikel_ID);
                    artikel.Add((a?.Artikel, artikelInRechnung.Count(b => b.Artikel_ID == a.Artikel_ID)));
                }
            }
            return (RechnungListe.Find(r => r.ID == id),artikel);
        }

        public List<Artikel> ArtikelList { get; set; }

        public List<ArtikelInRechnung> ArtikelInRechnungListe { get; set; }

        public List<Einheit> EinheitListe { get; set; }

        public List<Firma> FirmenListe { get; set; }

        public List<Kunde> KundenListe { get; set; }

        public List<Name> NameListe { get; set; }

        public List<Rabatt> RabattListe { get; set; }

        public List<Rechnung> RechnungListe { get; set; }

        public List<Sachbearbeiter> SachbearbeiterListe { get; set; }

        public List<Adresse> AdressenListe { get; set; }

        public List<Zahlungskonditionen> ZahlungskonditionenList { get; set; }
    }
}