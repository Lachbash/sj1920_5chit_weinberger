﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL2XML
{
    class Patient
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string SV { get; set; }

        public List<Behandlung> Behandlungen { get; set; }

        public Patient()
        {
            Behandlungen = new List<Behandlung>();
        }
    }
}
