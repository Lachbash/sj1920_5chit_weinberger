﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace SQL2XML
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private XmlDocument doc;
        private List<Patient> patienten;
        private List<Patient> xmlPatienten;
        private string conStr;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void DatabaseFileDialogButton_Click(object sender, RoutedEventArgs e)
        {

            xmlPatienten = new List<Patient>();

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Database (*.accdb)|*.accdb|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true);
            string test = openFileDialog.FileName;
            FillThePatientList(test);
            CreateXML();
            ReadDataFromXML();
            FillTheCheck();

        }

        private void FillThePatientList(string path)
        {
            conStr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source="+ path +"; Persist Security Info= False;";

            List<Patient> databasePatienten = new List<Patient>();
            List<Behandlung> behandlungKinds = new List<Behandlung>();

            using (OleDbConnection oleDbConnection = new OleDbConnection(conStr))
            {
                oleDbConnection.Open();
                OleDbDataReader reader = null;
                OleDbCommand command = new OleDbCommand("SELECT * FROM Patient");
                command.Connection = oleDbConnection;

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    databasePatienten.Add(new Patient { ID = (int)reader[0], LastName = reader[1].ToString(), FirstName = reader[2].ToString(), SV = reader[3].ToString() });
                }
            }

            using (OleDbConnection oleDbConnection = new OleDbConnection(conStr))
            {
                oleDbConnection.Open();
                OleDbDataReader reader = null;
                OleDbCommand command = new OleDbCommand("SELECT * FROM Behandlung");
                command.Connection = oleDbConnection;

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    behandlungKinds.Add(new Behandlung() { BehandlungsID = (int)reader[0], Bezeichnung = reader[1].ToString() });
                }
            }

            using (OleDbConnection oleDbConnection = new OleDbConnection(conStr))
            {
                oleDbConnection.Open();
                OleDbDataReader reader = null;
                OleDbCommand command = new OleDbCommand("SELECT * FROM PatientBehandlungen");
                command.Connection = oleDbConnection;

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Patient patient = databasePatienten.Find(x => x.ID == (int)reader[0]);
                    Behandlung behandlung = behandlungKinds.Find(x => x.BehandlungsID == (int)reader[1]);
                    patient.Behandlungen.Add(new Behandlung() { BehandlungsID = behandlung.BehandlungsID, Bezeichnung = behandlung.Bezeichnung, Duration = (int)reader[2], Date = (DateTime)reader[3] });
                }
            }

           patienten = databasePatienten;
        }

        private void CreateXML()
        {
            doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); //definition of the xml
            doc.AppendChild(docNode);

            XmlNode rootNode = doc.CreateElement("List");
            XmlAttribute dateAttribute = doc.CreateAttribute("Export");
            dateAttribute.Value = DateTime.Now.Date.ToShortDateString();

            

            //append the nodes
            doc.AppendChild(rootNode);
            rootNode.Attributes.Append(dateAttribute);


            foreach (Patient patient in patienten)
            {
                XmlNode nameNode = doc.CreateElement("Name");
                XmlAttribute idAttribute = doc.CreateAttribute("ID");
                idAttribute.Value = patient.ID.ToString();
                nameNode.Attributes.SetNamedItem(idAttribute);
                rootNode.AppendChild(nameNode);

                XmlNode firstNameNode = doc.CreateElement("Vorname");
                firstNameNode.InnerText = patient.FirstName;
                XmlNode lastNameNode = doc.CreateElement("Nachname");
                lastNameNode.InnerText = patient.LastName;
                XmlNode svNode = doc.CreateElement("SV");
                svNode.InnerText = patient.SV;

                nameNode.AppendChild(lastNameNode);
                nameNode.AppendChild(firstNameNode);
                nameNode.AppendChild(svNode);

                XmlNode behandlungenNode = doc.CreateElement("Behandlungen");
                nameNode.AppendChild(behandlungenNode);

                foreach (Behandlung behandlung in patient.Behandlungen)
                {
                    XmlNode behandlungNode = doc.CreateElement("Behandlung");
                    behandlungenNode.AppendChild(behandlungNode);

                    XmlNode bezeichnungNode = doc.CreateElement("Bezeichnung");
                    bezeichnungNode.InnerText = behandlung.Bezeichnung;
                    XmlNode dauerNode = doc.CreateElement("Dauer");
                    dauerNode.InnerText = behandlung.Duration.ToString();
                    XmlNode dateNode = doc.CreateElement("Datum");
                    dateNode.InnerText = behandlung.Date.ToShortDateString();

                    behandlungNode.AppendChild(bezeichnungNode);
                    behandlungNode.AppendChild(dauerNode);
                    behandlungNode.AppendChild(dateNode);
                }
            }

           

            doc.Save("Transfer.xml");
        }

        private void ReadDataFromXML()
        {
            doc = new XmlDocument();
            doc.LoadXml(File.ReadAllText("Transfer.xml"));

            XmlNodeList nodes = doc.SelectNodes("//Name");
            foreach (XmlNode item in nodes)
            {
                Patient patient = new Patient();
                patient.FirstName = item.SelectSingleNode("Vorname").InnerText;
                patient.LastName = item.SelectSingleNode("Nachname").InnerText;
                patient.SV = item.SelectSingleNode("SV").InnerText;
                patient.ID = Convert.ToInt32(item.Attributes["ID"].Value);

                XmlNode behandlungen = item.SelectSingleNode("Behandlungen");
                XmlNodeList nodeListBehandlung = behandlungen.SelectNodes("Behandlung");

                foreach (XmlNode behandlung in nodeListBehandlung)
                {
                    Behandlung be = new Behandlung();
                    be.Bezeichnung = behandlung.SelectSingleNode("Bezeichnung").InnerText;
                    be.Duration = Convert.ToInt32(behandlung.SelectSingleNode("Dauer").InnerText);
                    be.Date = Convert.ToDateTime(behandlung.SelectSingleNode("Datum").InnerText);
                    patient.Behandlungen.Add(be);
                }
                xmlPatienten.Add(patient);
            }
        }

        private void FillTheCheck()
        {

            aPXMl.Text = xmlPatienten.Count().ToString();
            aBXML.Text = GetBehandlungenCount(xmlPatienten);
            uBXML.Text = GetBehandlungenKinds(xmlPatienten);
            pMEBXML.Text = GetPatientsWithOneBehandlung(xmlPatienten);
            pMZBXML.Text = GetPatientsWithTwoBehandlung(xmlPatienten);
            pMMZBXML.Text = GetPatientsWithMoreThanTwoBehandlung(xmlPatienten);
            sTZXML.Text = GetSumTherapyTime(xmlPatienten);

            aPSQL.Text = patienten.Count().ToString();
            aBSQL.Text = GetBehandlungenCount(patienten);
            uBSQL.Text = GetBehandlungenKinds(patienten);
            pMEBSQL.Text = GetPatientsWithOneBehandlung(patienten);
            pMZBSQL.Text = GetPatientsWithTwoBehandlung(patienten);
            pMMZBSQL.Text = GetPatientsWithMoreThanTwoBehandlung(patienten);
            sTZSQL.Text = GetSumTherapyTime(patienten);

            if (CheckIfDataIsCorrect())
            {
                dataCheckVisualisation.Text = "Korrekt";
                dataCheckVisualisation.Background = Brushes.Green;
            }
            else
            {
                dataCheckVisualisation.Text = "Falsch";
                dataCheckVisualisation.Background = Brushes.Red;
            }

        }
        private bool CheckIfDataIsCorrect()
        {

            if (aPXMl.Text == aPSQL.Text &&
                aBXML.Text == aBSQL.Text &&
                uBXML.Text == uBSQL.Text &&
                pMEBXML.Text == pMEBSQL.Text &&
                pMZBXML.Text == pMZBSQL.Text &&
                pMMZBXML.Text == pMMZBSQL.Text &&
                sTZXML.Text == sTZSQL.Text)
            {
                return true;
            }
            return false;
        }

        private string GetSumTherapyTime(List<Patient> patienten)
        {
            int sum = 0;
            foreach (Patient patient in patienten)
            {
                foreach (Behandlung behandlung in patient.Behandlungen)
                {
                    sum += behandlung.Duration;
                }
            }

            return sum.ToString();
        }

        private string GetPatientsWithOneBehandlung(List<Patient> patienten)
        {
            int count = 0;

            foreach (Patient patient in patienten)
            {
                if (patient.Behandlungen.Count == 1)
                {
                    count++;
                }
            }

            return count.ToString();
        }

        private string GetPatientsWithTwoBehandlung(List<Patient> patienten)
        {
            int count = 0;

            foreach (Patient patient in patienten)
            {
                if (patient.Behandlungen.Count == 2)
                {
                    count++;
                }
            }

            return count.ToString();
        }

        private string GetPatientsWithMoreThanTwoBehandlung(List<Patient> patienten)
        {
            int count = 0;

            foreach (Patient patient in patienten)
            {
                if (patient.Behandlungen.Count > 2)
                {
                    count++;
                }
            }

            return count.ToString();
        }

        private string GetBehandlungenKinds(List<Patient> patienten)
        {
            List<Behandlung> behandlungsArten = new List<Behandlung>();
            bool note = true;
            foreach (Patient item in patienten)
            {
                foreach (Behandlung behandlung in item.Behandlungen)
                {
                    foreach (Behandlung notedBehandlung in behandlungsArten)
                    {
                        if (notedBehandlung.Bezeichnung == behandlung.Bezeichnung)
                        {
                            note = false;
                        }
                    }
                    if (note)
                    {
                        behandlungsArten.Add(behandlung);
                    }
                    else
                    {
                        note = true;
                    }
                }
            }

            return behandlungsArten.Count().ToString();
        }

        private string GetBehandlungenCount(List<Patient> patienten)
        {
            int behandlungenCounter = 0;
            foreach (Patient item in patienten)
            {
                behandlungenCounter += item.Behandlungen.Count();
            }

            return behandlungenCounter.ToString();
        }

    }
}
