﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace XML2SQL
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private XmlDocument doc;
        private List<Patient> patienten;
        private string conStr;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void selectionButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Xml files (*.xml)|*.xml|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
                doc.LoadXml(File.ReadAllText(openFileDialog.FileName));

            patienten.Clear();
            GetAllPatients();
            FillTheDataXML();
        }

        private void FillTheDataXML()
        {
            aPXMl.Text = patienten.Count().ToString();
            aBXML.Text = GetBehandlungenCount(patienten);
            uBXML.Text = GetBehandlungenKinds(patienten);
            pMEBXML.Text = GetPatientsWithOneBehandlung(patienten);
            pMZBXML.Text = GetPatientsWithTwoBehandlung(patienten);
            pMMZBXML.Text = GetPatientsWithMoreThanTwoBehandlung(patienten);
            sTZXML.Text = GetSumTherapyTime(patienten);
        }

        private void FillTheDataSQL()
        {
            List<Patient> patientenDatabase = GetDataFromDatabase();
            aPSQL.Text = patientenDatabase.Count().ToString();
            aBSQL.Text = GetBehandlungenCount(patientenDatabase);
            uBSQL.Text = GetBehandlungenKinds(patientenDatabase);
            pMEBSQL.Text = GetPatientsWithOneBehandlung(patientenDatabase);
            pMZBSQL.Text = GetPatientsWithTwoBehandlung(patientenDatabase);
            pMMZBSQL.Text = GetPatientsWithMoreThanTwoBehandlung(patientenDatabase);
            sTZSQL.Text = GetSumTherapyTime(patientenDatabase);
        }

        private List<Patient> GetDataFromDatabase()
        {
            List<Patient> databasePatienten = new List<Patient>();
            List<Behandlung> behandlungKinds = new List<Behandlung>();

            using (OleDbConnection oleDbConnection = new OleDbConnection(conStr))
            {
                oleDbConnection.Open();
                OleDbDataReader reader = null;
                OleDbCommand command = new OleDbCommand("SELECT * FROM Patient");
                command.Connection = oleDbConnection;

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    databasePatienten.Add(new Patient { ID = (int)reader[0], LastName = reader[1].ToString(), FirstName = reader[2].ToString(), SV = reader[3].ToString() });
                }
            }

            using (OleDbConnection oleDbConnection = new OleDbConnection(conStr))
            {
                oleDbConnection.Open();
                OleDbDataReader reader = null;
                OleDbCommand command = new OleDbCommand("SELECT * FROM Behandlung");
                command.Connection = oleDbConnection;

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    behandlungKinds.Add(new Behandlung() {BehandlungsID = (int)reader[0], Bezeichnung = reader[1].ToString() });
                }
            }

            using (OleDbConnection oleDbConnection = new OleDbConnection(conStr))
            {
                oleDbConnection.Open();
                OleDbDataReader reader = null;
                OleDbCommand command = new OleDbCommand("SELECT * FROM PatientBehandlungen");
                command.Connection = oleDbConnection;

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Patient patient = databasePatienten.Find(x => x.ID == (int)reader[0]);
                    Behandlung behandlung = behandlungKinds.Find(x => x.BehandlungsID == (int)reader[1]);
                    patient.Behandlungen.Add(new Behandlung() {BehandlungsID = behandlung.BehandlungsID, Bezeichnung = behandlung.Bezeichnung, Duration= (int)reader[2], Date = (DateTime)reader[3] });
                }
            }

            return databasePatienten;
        }

        private string GetSumTherapyTime(List<Patient> patienten)
        {
            int sum = 0;
            foreach (Patient patient in patienten)
            {
                foreach (Behandlung behandlung in patient.Behandlungen)
                {
                    sum += behandlung.Duration;
                }
            }

            return sum.ToString();
        }

        private string GetPatientsWithOneBehandlung(List<Patient> patienten)
        {
            int count = 0;

            foreach (Patient patient in patienten)
            {
                if (patient.Behandlungen.Count == 1)
                {
                    count++;
                }
            }

            return count.ToString();
        }

        private string GetPatientsWithTwoBehandlung(List<Patient> patienten)
        {
            int count = 0;

            foreach (Patient patient in patienten)
            {
                if (patient.Behandlungen.Count == 2)
                {
                    count++;
                }
            }

            return count.ToString();
        }

        private string GetPatientsWithMoreThanTwoBehandlung(List<Patient> patienten)
        {
            int count = 0;

            foreach (Patient patient in patienten)
            {
                if (patient.Behandlungen.Count > 2)
                {
                    count++;
                }
            }

            return count.ToString();
        }

        private string GetBehandlungenKinds(List<Patient> patienten)
        {
            List<Behandlung> behandlungsArten = new List<Behandlung>();
            bool note = true;
            foreach (Patient item in patienten)
            {
                foreach (Behandlung behandlung in item.Behandlungen)
                {
                    foreach (Behandlung notedBehandlung in behandlungsArten)
                    {
                        if (notedBehandlung.Bezeichnung == behandlung.Bezeichnung)
                        {
                            note = false;
                        }
                    }
                    if (note)
                    {
                        behandlungsArten.Add(behandlung);
                    }
                    else
                    {
                        note = true;
                    }
                }
            }

            return behandlungsArten.Count().ToString();
        }

        private string GetBehandlungenCount(List<Patient> patienten)
        {
            int behandlungenCounter = 0;
            foreach (Patient item in patienten)
            {
                behandlungenCounter += item.Behandlungen.Count();
            }

            return behandlungenCounter.ToString();
        }

        private void GetAllPatients()
        {
            XmlNodeList nodes = doc.SelectNodes("//Name");
            foreach (XmlNode item in nodes)
            {
                Patient patient = new Patient();
                patient.FirstName = item.SelectSingleNode("Vorname").InnerText;
                patient.LastName = item.SelectSingleNode("Nachname").InnerText;
                patient.SV = item.SelectSingleNode("SV").InnerText;
                patient.ID = Convert.ToInt32(item.Attributes["ID"].Value);

                XmlNode behandlungen = item.SelectSingleNode("Behandlungen");
                XmlNodeList nodeListBehandlung = behandlungen.SelectNodes("Behandlung");

                foreach (XmlNode behandlung in nodeListBehandlung)
                {
                    Behandlung be = new Behandlung();
                    be.Bezeichnung = behandlung.SelectSingleNode("Bezeichnung").InnerText;
                    be.Duration = Convert.ToInt32(behandlung.SelectSingleNode("Dauer").InnerText);
                    be.Date = Convert.ToDateTime(behandlung.SelectSingleNode("Datum").InnerText);
                    patient.Behandlungen.Add(be);
                }
                patienten.Add(patient);
            }
        }

        private void XML2SQL_Loaded(object sender, RoutedEventArgs e)
        {
            doc = new XmlDocument();
            patienten = new List<Patient>();
        }

        private void TransferIntoDatabase_Click(object sender, RoutedEventArgs e)
        {
            if (patienten.Count != 0)
            {
                conStr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Transfer.accdb; Persist Security Info= False;";

                ClearDatabase();
                WritePatientsIntoDatabase();
                WriteBehandlungenIntoDatabase();
                WritePatientBehandlungenIntoDatabase();

                MessageBox.Show("finished Transfer");
                FillTheDataSQL();
                if (CheckIfDataIsCorrect())
                {
                    dataCheckVisualisation.Text = "Korrekt";
                    dataCheckVisualisation.Background = Brushes.Green;
                }
                else
                {
                    dataCheckVisualisation.Text = "Falsch";
                    dataCheckVisualisation.Background = Brushes.Red;
                }
                
            }
        }

        private bool CheckIfDataIsCorrect()
        {
            //aPXMl.Text = patienten.Count().ToString();
            //aBXML.Text = GetBehandlungenCount(patienten);
            //uBXML.Text = GetBehandlungenKinds(patienten);
            //pMEBXML.Text = GetPatientsWithOneBehandlung(patienten);
            //pMZBXML.Text = GetPatientsWithTwoBehandlung(patienten);
            //pMMZBXML.Text = GetPatientsWithMoreThanTwoBehandlung(patienten);
            //sTZXML.Text = GetSumTherapyTime(patienten);

            if (aPXMl.Text == aPSQL.Text && 
                aBXML.Text ==aBSQL.Text && 
                uBXML.Text == uBSQL.Text && 
                pMEBXML.Text == pMEBSQL.Text &&
                pMZBXML.Text == pMZBSQL.Text &&
                pMMZBXML.Text == pMMZBSQL.Text &&
                sTZXML.Text == sTZSQL.Text)
            {
                return true;
            }
            return false;
        }

        private void WritePatientBehandlungenIntoDatabase()
        {
            foreach (Patient item in patienten)
            {
                foreach (Behandlung behandlung in item.Behandlungen)
                {
                    string comm = "INSERT INTO PatientBehandlungen VALUES ('" + item.ID + "', '" + behandlung.BehandlungsID + "', '" + behandlung.Duration + "', '" + behandlung.Date.Date + "');";
                    //string comm = "INSERT INTO Patient VALUES (" + item.ID + ", " + item.FirstName + ", " + item.LastName + ", " + item.SV + ");";


                    using (OleDbConnection connection = new OleDbConnection(conStr))
                    {
                        // The insertSQL string contains a SQL statement that
                        // inserts a new row in the source table.
                        OleDbCommand command = new OleDbCommand(comm);

                        // Set the Connection to the new OleDbConnection.
                        command.Connection = connection;

                        // Open the connection and execute the command.
                        try
                        {
                            connection.Open();
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            break;
                        }
                    }
                }
            }
        }

        private void WriteBehandlungenIntoDatabase()
        {
            List<Behandlung> behandlungsArten = new List<Behandlung>();
            bool note = true;
            foreach (Patient item in patienten)
            {
                foreach (Behandlung behandlung in item.Behandlungen)
                {
                    foreach (Behandlung notedBehandlung in behandlungsArten)
                    {
                        if (notedBehandlung.Bezeichnung == behandlung.Bezeichnung)
                        {
                            note = false;
                        }
                    }
                    if (note)
                    {
                        behandlungsArten.Add(behandlung);
                    }
                    else
                    {
                        note = true;
                    }
                }
            }

            for (int i = 0; i < behandlungsArten.Count(); i++)
            {
                behandlungsArten[i].BehandlungsID = i;
            }

            foreach (Patient item in patienten)
            {
                foreach (Behandlung behandlung in item.Behandlungen)
                {
                    foreach (Behandlung behandlungsArt in behandlungsArten)
                    {
                        if (behandlung.Bezeichnung == behandlungsArt.Bezeichnung)
                        {
                            behandlung.BehandlungsID = behandlungsArt.BehandlungsID;
                        }
                    }
                }
            }

            foreach (Behandlung item in behandlungsArten)
            {

                string comm = "INSERT INTO Behandlung VALUES ('" + item.BehandlungsID + "', '" + item.Bezeichnung +"');";
                //string comm = "INSERT INTO Patient VALUES (" + item.ID + ", " + item.FirstName + ", " + item.LastName + ", " + item.SV + ");";


                using (OleDbConnection connection = new OleDbConnection(conStr))
                {
                    // The insertSQL string contains a SQL statement that
                    // inserts a new row in the source table.
                    OleDbCommand command = new OleDbCommand(comm);

                    // Set the Connection to the new OleDbConnection.
                    command.Connection = connection;

                    // Open the connection and execute the command.
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        break;
                    }
                }
            }
        }

        private void WritePatientsIntoDatabase()
        {
            foreach (Patient item in patienten)
            {

                string comm = "INSERT INTO Patient VALUES ('" + item.ID + "', '" + item.LastName + "', '" + item.FirstName + "', '" + item.SV + "');";
                //string comm = "INSERT INTO Patient VALUES (" + item.ID + ", " + item.FirstName + ", " + item.LastName + ", " + item.SV + ");";


                using (OleDbConnection connection = new OleDbConnection(conStr))
                {
                    // The insertSQL string contains a SQL statement that
                    // inserts a new row in the source table.
                    OleDbCommand command = new OleDbCommand(comm);

                    // Set the Connection to the new OleDbConnection.
                    command.Connection = connection;

                    // Open the connection and execute the command.
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        break;
                    }
                }
            }
        }

        private void ClearDatabase()
        {
            using (OleDbConnection delConn = new OleDbConnection(conStr))
            {
                delConn.Open();
                String delQuery = "DELETE * FROM Behandlung";
                OleDbCommand delcmd = new OleDbCommand(delQuery);
                delcmd.Connection = delConn;
                delcmd.ExecuteNonQuery();
                delQuery = "DELETE * FROM Patient";
                delcmd = new OleDbCommand(delQuery);
                delcmd.Connection = delConn;
                delcmd.ExecuteNonQuery();
                delQuery = "DELETE * FROM PatientBehandlungen";
                delcmd = new OleDbCommand(delQuery);
                delcmd.Connection = delConn;
                delcmd.ExecuteNonQuery();

            }

        }
    }
}
