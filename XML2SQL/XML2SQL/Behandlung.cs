﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XML2SQL
{
    class Behandlung
    {
        public string Bezeichnung { get; set; }

        public int Duration { get; set; }

        public DateTime Date { get; set; }

        public int BehandlungsID { get; set; }
    }
}
