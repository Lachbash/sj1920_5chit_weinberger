﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MillionenshowServer
{
    class GameToken
    {
        public int GameID { get; set; }

        public int ID { get; set; }

        private List<Question> questionHistory;

        public List<int> answeres { get; set; }

        public Question ActualQuestion
        {
            get { return actualQuestion; }
            set {
               questionHistory.Add(ActualQuestion);
               actualQuestion = value; 
            }
        }

        private Question actualQuestion;

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public GameToken()
        {
            answeres = new List<int>();
            questionHistory = new List<Question>();
        }

        public GameToken(int gameID, int ID, Question question, DateTime startTime)
        {
            answeres = new List<int>();
            questionHistory = new List<Question>();
            this.GameID = gameID;
            this.ID = ID;
            actualQuestion = question;
            StartTime = startTime;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(ID+"\nGame #"+GameID+"\nBeginn: "+StartTime.ToLongTimeString()+"\n\n");
            for (int i = 0; i < questionHistory.Count; i++)
            {
                char[] answerMarker = new char[5];
                for (int j = 0; j < answerMarker.Length; j++)
                {
                    if (answeres[i] == j)
                    {
                        answerMarker[j] = '*';
                    }
                    else
                    {
                        answerMarker[j] = ' ';
                    }
                }
                sb.Append("Frage: " + questionHistory[i].QuestionString + "\n");
                sb.Append("\t"+answerMarker[1]+ questionHistory[i].Answer1 + "\n");
                sb.Append("\t"+answerMarker[2] + questionHistory[i].Answer2 + "\n");
                sb.Append("\t"+answerMarker[3] + questionHistory[i].Answer3 + "\n");
                sb.Append("\t"+answerMarker[4] + questionHistory[i].Answer4 + "\n");
            }
            if (EndTime != null)
            {
                sb.Append("Ende: " + EndTime.ToLongTimeString());
            }
            else
            {
                sb.Append("Ende: Game still in progress");
            }
            
            return sb.ToString();
        }
    }
}
