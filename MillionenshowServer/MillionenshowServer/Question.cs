﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MillionenshowServer
{
    class Question
    {
        public String Answer1 { get; }

        public String Answer2 { get; }

        public String Answer3 { get; }

        public String Answer4 { get; }

        public String QuestionString { get; }

        public List<int> CorrectIndex { get; }

        public QuestionType QuestionType { get; }

        public Question(string question ,string answer1, string answer2, string answer3, string answer4, List<int> correctIndex, QuestionType questionType)
        {
            QuestionString = question;
            Answer1 = answer1;
            Answer2 = answer2;
            Answer3 = answer3;
            Answer4 = answer4;
            CorrectIndex = correctIndex;
            QuestionType = questionType;
        }
    }
}
