﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Threading;

namespace MillionenshowServer
{
  
    class Server
    {
        
        const string SERVER_IP = "127.0.0.1";
        private TcpListener listener;
        private Game game;
        private bool running;
        private Thread t;

        public Server(int maxClients, int port)
        {
            game = new Game(maxClients);
            running = true;
            SettingUpServer(maxClients, port);
            t = new Thread(new ThreadStart(ServerCommunication));
            t.Start();
            ReadKeyInput();
            
            //ServerCommunication();
        }

        private void ServerCommunication()
        {
            while (running) 
            {

                //Waiting for Clients
                TcpClient client = listener.AcceptTcpClient();

                NetworkStream nwStream = client.GetStream();
                byte[] buffer = new byte[client.ReceiveBufferSize];
                int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);
                string dataReceived = Encoding.Default.GetString(buffer, 0, bytesRead);
                //Console.WriteLine("Server-Received : " + dataReceived);

                String message = StartResponseProcess(dataReceived);
                //Console.WriteLine("Server-Sending back: " + message);
                byte[] bytesSend = ASCIIEncoding.Default.GetBytes(message);  //prepare the String which should be sent
                nwStream.Write(bytesSend, 0, bytesSend.Length); // sending the Client the ID
                client.Close();

            }

            listener.Stop();
            Console.ReadLine();
        }

        private void ReadKeyInput()
        {
            HeaderMessage();
            ConsoleKey choice;
            while (true)
            {
                choice = Console.ReadKey(true).Key;
                switch (choice)
                {

                    case ConsoleKey.E:
                        HeaderMessage();
                        running = false;
                        t.Interrupt();
                        Environment.Exit(0);
                        break;

                    case ConsoleKey.NumPad1:
                    case ConsoleKey.D1:
                        HeaderMessage();
                        listTokens();
                        Console.WriteLine("\nold games:\n\n");
                        listOldTokens();
                        break;
                }
            }
        }

        private void listOldTokens()
        {
            bool atLeastOne = false;
            for (int i = 0; i < game.ClientsHistory.Count; i++)
            {
                if (game.ClientsHistory[i] != null)
                {
                    Console.WriteLine(game.ClientsHistory[i]);
                    atLeastOne = true;
                }
            }
            if (!atLeastOne)
            {
                Console.WriteLine("No old Tokens found!");
            }
        }

        private static void HeaderMessage()
        {
            Console.Clear();
            Console.WriteLine("--------------------------------");
            Console.WriteLine("1...Aktuelle Statistik ausgeben!");
            Console.WriteLine("E...Server beenden");
            Console.WriteLine("--------------------------------");
        }

        private void listTokens()
        {
            bool atLeastOne = false;
            for (int i = 0; i < game.Clients.Length; i++)
            {
                if (game.Clients[i] != null)
                {
                    Console.WriteLine(game.Clients[i]);
                    atLeastOne = true;
                }
            }
            if (!atLeastOne)
            {
                Console.WriteLine("No Tokens avaiable!");
            }
        }

        private String StartResponseProcess(string dataReceived)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(dataReceived);
            XmlNode element = doc.SelectSingleNode("REQUEST");
            XmlNode gameIDElement = element.SelectSingleNode("GAMEID");
            XmlNode answerIDElement = element.SelectSingleNode("ANSWERID");
            element = element.SelectSingleNode("ID");
            switch (element.InnerText)
            {
                case "1":
                    return CreatingQuestionMessage(game.randomQuestion(), 1);
                    break;
                case "2":
                    try
                    {
                        int gameID = Convert.ToInt32(gameIDElement.InnerText);
                        return NewQuestionMessage(game.randomQuestion(), 2, gameID);
                    }
                    catch (Exception)
                    {
                        return "WrongMessageFormat";
                    }
                    break;
                case "3":
                    try
                    {
                        int gameID = Convert.ToInt32(gameIDElement.InnerText);
                        int answerID = Convert.ToInt32(answerIDElement.InnerText);
                        return checkAnswerMessage(game.randomQuestion(), 3, gameID, answerID);
                    }
                    catch (Exception)
                    {
                        return "WrongMessageFormat";
                    }
                    break;
                case "4":
                    try
                    {
                        int gameID = Convert.ToInt32(gameIDElement.InnerText);
                        return createEndMessage(4, gameID);
                    }
                    catch (Exception)
                    {
                        return "WrongMessageFormat";
                    }
                    break;
                default:
                    return "Wrong Message Fromat";
                    break;
            }
            
        }

        private string createEndMessage(int ID, int gameID)
        {
            GameToken gameToken = TokenGeneration(gameID);
            gameToken.ID = ID;
            gameToken.EndTime = DateTime.Now;

            XmlDocument doc = new XmlDocument();

            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); //definition of the xml
            doc.AppendChild(docNode);

            XmlNode rootNode = doc.CreateElement("RESPONSE");
            doc.AppendChild(rootNode);

            XmlNode idNode = doc.CreateElement("ID");
            idNode.InnerText = gameToken.ID.ToString();
            rootNode.AppendChild(idNode);

            XmlNode gameIDNode = doc.CreateElement("GAMEID");
            gameIDNode.InnerText = gameToken.GameID.ToString();
            rootNode.AppendChild(gameIDNode);

            XmlNode beginTimeNode = doc.CreateElement("BEGIN");
            beginTimeNode.InnerText = gameToken.StartTime.ToLongTimeString();
            rootNode.AppendChild(beginTimeNode);

            XmlNode endTimeNode = doc.CreateElement("END");
            endTimeNode.InnerText = gameToken.EndTime.ToLongTimeString();
            rootNode.AppendChild(endTimeNode);

            game.ClientsHistory.Add(game.Clients[gameToken.GameID]);
            game.Clients[gameToken.GameID] = null;

            return doc.OuterXml;
        }

        private string checkAnswerMessage(Question questionGroup, int ID, int gameID, int answerID)
        {
            XmlDocument doc = new XmlDocument();
            GameToken gameToken = TokenGeneration(gameID);
            gameToken.ID = ID;
            gameToken.answeres.Add(answerID);

            String correctAnswer = "false";
            if (answerID == gameToken.ActualQuestion.CorrectIndex[0])
            {
                correctAnswer = "true";
            }

            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); //definition of the xml
            doc.AppendChild(docNode);

            XmlNode rootNode = doc.CreateElement("RESPONSE");
            doc.AppendChild(rootNode);

            XmlNode idNode = doc.CreateElement("ID");
            idNode.InnerText = gameToken.ID.ToString();
            rootNode.AppendChild(idNode);

            XmlNode gameIDNode = doc.CreateElement("GAMEID");
            gameIDNode.InnerText = gameToken.GameID.ToString();
            rootNode.AppendChild(gameIDNode);

            XmlNode correctnessIDNode = doc.CreateElement("CORRECT");
            correctnessIDNode.InnerText = correctAnswer;
            rootNode.AppendChild(correctnessIDNode);

            return doc.OuterXml;
        }

        private string NewQuestionMessage(Question questionGroup, int ID, int gameID)
        {
            GameToken gameToken = TokenGeneration(gameID);
            gameToken.ID = ID;
            gameToken.ActualQuestion = questionGroup;

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); //definition of the xml
            doc.AppendChild(docNode);

            XmlNode rootNode = doc.CreateElement("RESPONSE");
            doc.AppendChild(rootNode);

            XmlNode idNode = doc.CreateElement("ID");
            idNode.InnerText = gameToken.ID.ToString();
            rootNode.AppendChild(idNode);

            XmlNode gameIDNode = doc.CreateElement("GAMEID");
            gameIDNode.InnerText = gameToken.GameID.ToString();
            rootNode.AppendChild(gameIDNode);

            XmlNode questionNode = doc.CreateElement("QUESTION");
            questionNode.InnerText = questionGroup.QuestionString;
            rootNode.AppendChild(questionNode);

            XmlNode levelNode = doc.CreateElement("LEVEL");
            levelNode.InnerText = questionGroup.QuestionType.ToString();
            rootNode.AppendChild(levelNode);

            XmlNode answer1Node = doc.CreateElement("ANSWER1");
            answer1Node.InnerText = questionGroup.Answer1;
            rootNode.AppendChild(answer1Node);

            XmlNode answer2Node = doc.CreateElement("ANSWER2");
            answer2Node.InnerText = questionGroup.Answer2;
            rootNode.AppendChild(answer2Node);

            XmlNode answer3Node = doc.CreateElement("ANSWER3");
            answer3Node.InnerText = questionGroup.Answer3;
            rootNode.AppendChild(answer3Node);

            XmlNode answer4Node = doc.CreateElement("ANSWER4");
            answer4Node.InnerText = questionGroup.Answer4;
            rootNode.AppendChild(answer4Node);

            return doc.OuterXml;
        }

        private void SettingUpServer(int maxClients, int port)
        {
            //Setting up the Server
            IPAddress localAdd = IPAddress.Parse(SERVER_IP);
            listener = new TcpListener(localAdd, port);
            listener.Start();
        }

        private String CreatingQuestionMessage(Question questionGroup, int ID)
        {
            GameToken gameToken = TokenGeneration(questionGroup, ID);

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); //definition of the xml
            doc.AppendChild(docNode);

            XmlNode rootNode = doc.CreateElement("RESPONSE");
            doc.AppendChild(rootNode);

            XmlNode idNode = doc.CreateElement("ID");
            idNode.InnerText = gameToken.ID.ToString();
            rootNode.AppendChild(idNode);

            XmlNode gameID = doc.CreateElement("GAMEID");
            gameID.InnerText = gameToken.GameID.ToString();
            rootNode.AppendChild(gameID);

            XmlNode question = doc.CreateElement("QUESTION");
            question.InnerText = questionGroup.QuestionString;
            rootNode.AppendChild(question);

            XmlNode level = doc.CreateElement("LEVEL");
            level.InnerText = questionGroup.QuestionType.ToString();
            rootNode.AppendChild(level);

            XmlNode answer1 = doc.CreateElement("ANSWER1");
            answer1.InnerText = questionGroup.Answer1;
            rootNode.AppendChild(answer1);

            XmlNode answer2 = doc.CreateElement("ANSWER2");
            answer2.InnerText = questionGroup.Answer2;
            rootNode.AppendChild(answer2);

            XmlNode answer3 = doc.CreateElement("ANSWER3");
            answer3.InnerText = questionGroup.Answer3;
            rootNode.AppendChild(answer3);

            XmlNode answer4 = doc.CreateElement("ANSWER4");
            answer4.InnerText = questionGroup.Answer4;
            rootNode.AppendChild(answer4);

            return doc.OuterXml;

        }

        private GameToken TokenGeneration(Question questionGroup, int ID)
        {
            int gameID = game.FindToken();
            GameToken gameToken = new GameToken(gameID, ID, questionGroup, DateTime.Now);
            game.Clients[gameID] = gameToken;
            return gameToken;
        }

        private GameToken TokenGeneration(int gameID)
        {
            return game.FindToken(gameID);
        }
    }
}
