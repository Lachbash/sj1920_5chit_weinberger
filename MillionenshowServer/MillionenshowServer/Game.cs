﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MillionenshowServer
{
    class Game
    {
        public List<Question> questions { get; set; }
        private Random rand;
        public GameToken[] Clients { get; set; }
        public List<GameToken> ClientsHistory;
        public Game(int clients)
        {
            ClientsHistory = new List<GameToken>();
            rand = new Random();
            questions = new List<Question>();
            loadQuestions();
            Clients = new GameToken[clients];
        }

        public void loadQuestions()
        {
            String[] lines;
            try
            {
                lines = System.IO.File.ReadAllLines("fragen.txt");
            }
            catch (Exception e)
            {
                Console.WriteLine("couldn't read questions from File");
                Console.WriteLine(e.Message);
                throw;
            }



            for (int i = 0; i < lines.Length; i += 6)
            {
                String question = lines[1+i];
                QuestionType questionType = (QuestionType) Convert.ToInt32(lines[0+i]);
                List<int> correctIndex = new List<int>();

                int counter = 0;
                for (int j = 2; j < 6; j++)
                {
                    
                    if (lines[i+j][0] == '■')
                    {
                        char s = lines[i + j][0];
                        lines[i + j] = lines[i + j].TrimStart('■');
                        correctIndex.Add(j-1);
                    }
                    else
                    {
                        char s = lines[i + j][0];
                        counter++;
                    }
                }
                if (counter != 4)
                {
                    String answerA = lines[2+i];
                    String answerB = lines[3+i];
                    String answerC = lines[4+i];
                    String answerD = lines[5+i];
                    Question questionGroup = new Question(question, answerA, answerB, answerC, answerD, correctIndex, questionType);
                    questions.Add(questionGroup);
                }
            }
        }

        public Question randomQuestion()
        {
            int random = rand.Next(0, questions.Count());
            return questions[random];
        }

        public int FindToken()
        {
            for (int i = 0; i < Clients.Length; i++)
            {
                if (Clients[i] == null)
                {
                    return i;
                }
            }
            return -1;
        }

        public GameToken FindToken(int gameID)
        {
            for (int i = 0; i < Clients.Length; i++)
            {
                if (Clients[i].GameID == gameID)
                {
                    return Clients[i];
                }
            }
            return null;
        }
    }
}
