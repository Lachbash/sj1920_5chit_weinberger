﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Xml;

namespace HangManClient
{
    public class Client
    {
        private TcpClient client;
        private NetworkStream nwStream;
        private XmlDocument doc;
        private bool setID;
        const int TRIES = 20;
        public int Tries { get; set; }

        public int id { get; set; }

        private char[] letters{ get; set;}

        public string word { get; set; }

        public string IP { get; set; }

        public Client()
        {
            DeclareMessage();
            IP = "127.0.0.1";
            Tries = TRIES;
        }


        public void Connect()
        {
            client = new TcpClient(IP, 8888);
            nwStream = client.GetStream();
            
            //DeclareMessage();
            //SendToServer();
            //Console.ReadLine();
            //EndConnection();

        }

        public bool LetterGuess(char letter)
        {
           
           if (Char.IsLetter(letter)) //checks if the char is a letter
           {
                SendLetterToServer(letter);
                return true;
           }
           else //if not send back that no try was used, because it was no char
           {
                return false;
           }
        }
        
        public void SendLetterToServer(char letter)
        {
            string helper = letter.ToString().ToUpper();
            XmlNode node = doc.SelectSingleNode("//letter");
            node.InnerText = letter.ToString().ToUpper(); // Adds the letter to the message
            doc.Save("ClientMessage.xml");
            RenewConnection();
            SendToServer();
            WaitForServerReply();
        }

        private void RenewConnection()
        {
            EndConnection();
            client = new TcpClient(IP, 8888);
            nwStream = client.GetStream();
        }

        public string WaitForServerReply()
        {
            byte[] bytesToRead = new byte[client.ReceiveBufferSize];
            int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize); //reading the reply from the server
            //Console.WriteLine("Client-Received : " + Encoding.ASCII.GetString(bytesToRead, 0, bytesRead)); DEBUG INFO
            //id = Convert.ToInt32(Encoding.ASCII.GetString(bytesToRead, 0, bytesRead)); // assining the ID
            ReadMessage(Encoding.Default.GetString(bytesToRead, 0, bytesRead));

            return Encoding.Default.GetString(bytesToRead, 0, bytesRead);
            
        }

        public void ReadMessage(string recievedMessage)
        {
            try
            {
                doc.LoadXml(recievedMessage); //loads the Data into the doc
            }
            catch (Exception ex)
            {
                Console.WriteLine(recievedMessage);
                Console.WriteLine(ex.Message);
                Console.WriteLine("Can't Connect to Server");
                throw;
                
            }
            if (!setID)
            {
                XmlNode node = doc.SelectSingleNode("//id");

                if (node != null)
                {
                  if (node.InnerText != "") //checks if the client gets an id from the server
                  {
                        id = Convert.ToInt32(node.InnerText);
                        setID = true;
                  }
                }
                else
                {
                     //handle a case where something wrong happens
                }
            }

            if (setID)
            {
                XmlNode node = doc.SelectSingleNode("//word");
                
                if (node.InnerText.ToString() == word)
                {
                    Tries--;
                }

                word = node.InnerText.ToString(); // renews the word of the client
            }

            
        }

        private string GetMessageString()
        {
            return doc.OuterXml;
        }

        public void DeclareMessage()
        {
            //generating the first Message
            doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null); //definition of the xml
            doc.AppendChild(docNode);

            //building the xml
            XmlNode rootNode = doc.CreateElement("HangMan");
            XmlNode idNode = doc.CreateElement("id");
            XmlNode letterNode = doc.CreateElement("letter");
            
            if (id != 0)
            {
                idNode.InnerText = id.ToString();
            }

            //append the nodes
            doc.AppendChild(rootNode);
            rootNode.AppendChild(idNode);
            rootNode.AppendChild(letterNode);

            doc.Save("ClientMessage.xml");
            
        }

        public void ReConnect()
        {
            throw new System.NotImplementedException();
        }

        public void EndConnection()
        {
            if (client != null && nwStream != null) //only wenn there is a connection you are able to quit
            {
                //Message to the Server that it can clear the id
                client.Close();
            }
            
        }
        public string SendToServer()
        {
            string message = GetMessageString();
            byte[] bytesToSend = ASCIIEncoding.Default.GetBytes(message); //create the message for the server

            //Console.WriteLine("Client-Message: " + message); DEBUG INFO
            nwStream.Write(bytesToSend, 0, bytesToSend.Length); //sending the message to the server

            return message;
        }

        public void ResetTries()
        {
            Tries = TRIES;
        }
    }
}
