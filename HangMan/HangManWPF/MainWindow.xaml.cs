﻿using HangManClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HangManWPF
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Client client;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            client = new Client();
            client.Connect();
            client.DeclareMessage();
            client.SendToServer();
            client.WaitForServerReply();

            connectionStatusLable.Content = "Connected to : " + client.IP;
            guid.Text = "Hey, I' m your guid! \nI will tell you what to do or what happend\nTo get started just push any alphabetic Key on your Keyboard";
            hangManWord.Text = WriteWithSpace(client.word);
            //hangManWord.HorizontalAlignment = HorizontalAlignment.Center;
        }

        private string WriteWithSpace(string word)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < word.Length; i++)
            {
                sb.Append(word[i] + " ");
            }

            return sb.ToString();
        }

        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if ((Char.IsLetter(e.Key.ToString()[0]) && e.Key.ToString().Length == 1) || e.Key.Equals(Key.Oem7) || e.Key.Equals(Key.Oem1) || e.Key.Equals(Key.Oem3))
            {
                if (e.Key.Equals(Key.Oem7))
                {
                    client.SendLetterToServer('Ä');
                }
                else if (e.Key.Equals(Key.Oem1))
                {
                    client.SendLetterToServer('Ü');
                }
                else if (e.Key.Equals(Key.Oem3))
                {
                    client.SendLetterToServer('Ö');
                }
                else
                    client.SendLetterToServer(e.Key.ToString()[0]);

                guid.Text = "That was a letter \n";
                guid.Text += client.Tries.ToString() + " Tries left";
                hangManWord.Text = WriteWithSpace(client.word);
                if (!client.word.Contains("_"))
                {
                    guid.Text = "Congrats! YOU WON!\n To start a new game push the new Game Button";
                    Button b = new Button();
                    b.Click += new_game;
                    b.Content = "Congrats! YOU WON!\nnew Game";
                    hangManGrid.Children.Add(b);
                }

                if (client.Tries <= 0)
                {
                    guid.Text = "Sadly you lost!\nTo start a new game push the new Game Button";
                    Button b = new Button();
                    b.Click += new_game;
                    b.Content = "Sadly you lost!\nnew Game";
                    hangManGrid.Children.Add(b);
                }
            }

            else
            {
                guid.Text = "That wasn't a letter ";
            }
            
        }
        private void new_game(object sender, RoutedEventArgs e)
        {
            client.EndConnection();
            client.word = "";
            client.ResetTries();
            client.Connect();
            client.DeclareMessage();
            client.SendToServer();
            client.WaitForServerReply();

            hangManGrid.Children.Remove((Button)sender);
            hangManWord.Text = WriteWithSpace(client.word);
            guid.Text = "Hey, I' m your guid! \nI will tell you what to do or what happend\nTo get started just push any alphabetic Key on your Keyboard";
        }
        private void click_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
