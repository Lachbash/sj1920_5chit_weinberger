﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HangManClient;

namespace TestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client();
            client.Connect();
            client.DeclareMessage();
            client.SendToServer();
            client.WaitForServerReply();
            //Console.ReadLine();
            Console.WriteLine("LETS PLAY HANGMAN \n");
            Console.WriteLine("type your letter - just press any alphabetic button on the keyboards\n");
            Console.WriteLine("your word: "+client.word);

            while(client.Tries > 0)
            {
                int i = 0;

                if (!client.LetterGuess(Console.ReadKey().KeyChar))
                {
                    
                    Console.WriteLine("\nThat was no letter");
                }
                else
                {
                    Console.WriteLine("\nTries left: "+(client.Tries) +" - try number: ");
                    Console.WriteLine(client.word);
                }


                if (!client.word.Contains("_"))
                    break;
            }

            if (client.word.Contains("_"))
            {
                Console.WriteLine("\nNo tries left");
                Console.WriteLine("GAME OVER");
            }

            else
            {
                Console.WriteLine("\nYOU WON");
            }
           
            Console.ReadLine();
        }
    }
}
