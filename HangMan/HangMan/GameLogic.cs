﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HangMan
{
    public sealed class GameLogic
    {
        private static readonly GameLogic instance = new GameLogic();

        public static GameLogic Instance { get { return instance; }}
        // sagt dem Compiler den Type nicht als Beforefieldinit anzusehen 
        static GameLogic()
        {

        }

        private GameLogic()
        {

        }
        public List<string> words{ get; set; }
        public void getWords()
        {
            words = new List<string>();
            using (StreamReader sr = new StreamReader("words.txt")) // read words from txt
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    words.Add(line);
                }
            }
        }

        public string randomWord()
        {
            Random random = new Random();
            return words[random.Next(0, words.Count)].ToUpper();
        }

        public string hideWord(string word) 
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < word.Length; i++)
            {
                sb.Append("_");
            }

            return sb.ToString();
        }
        
        public int[] findLetterInWord (string word, char letter)
        {
            List<int> occurrences = new List<int>();
            if (word.Contains(letter.ToString())) // only search the positions if the char is in the word
            {
                for (int i = 0; i < word.Length; i++)
                {
                    if (word[i] == letter)
                    {
                        occurrences.Add(i);
                    }
                }
            }

            return occurrences.ToArray();
        }
    }
}