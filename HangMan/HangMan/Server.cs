﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;
using HangManClient;

namespace HangMan
{
    public class Server
    {
        XmlDocument doc;
        private Socket socket;
        private string message;
        const string SERVER_IP = "127.0.0.1";
        //public List<Client> clients { get; set; }
        public Client[] clients;

        public int MaxClients
        {
            get => default;
            set
            {
            }
        }

        private TcpListener listener;

        //private GameLogic gameLogic;

        public Server(int maxClients, int port)
        {
            SettingUpServer(maxClients, port);
            clients = new Client[maxClients];
            GameLogic.Instance.getWords();
            Console.WriteLine("Waiting for Clients:");
            HandleClient();
            listener.Stop();
            Console.ReadLine();
        }

        private void SettingUpServer(int maxClients, int port)
        {
            //Setting up the Server
            IPAddress localAdd = IPAddress.Parse(SERVER_IP);
            listener = new TcpListener(localAdd, port);
            this.MaxClients = maxClients;
            listener.Start();

            Console.WriteLine("Server starting Up");
        }

        public Client createNewGame()
        {
            throw new System.NotImplementedException();
        }

        public void HandleClient()
        {
            while (true) //attempt to let more Clients Join
            {
                //Waiting for Clients
                TcpClient client = listener.AcceptTcpClient();

                NetworkStream nwStream = client.GetStream();
                byte[] buffer = new byte[client.ReceiveBufferSize];
                int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);
                string dataReceived = Encoding.Default.GetString(buffer, 0, bytesRead);
                Console.WriteLine("Server-Received : " + dataReceived);

                GenerateMessage(dataReceived);
                Console.WriteLine("Server-Sending back: " + message);
                byte[] bytesSend = ASCIIEncoding.Default.GetBytes(message);  //prepare the String which should be sent
                nwStream.Write(bytesSend, 0, bytesSend.Length); // sending the Client the ID
                client.Close();

            }
        }

        public void RemoveClient(int id)
        {
            //Client removeClient = clients.Find(x => x.id == id); //find the client with the id
            //clients.Remove(removeClient);   //remove the client
            clients[id] = null;
        }

        public int SendClientResult()
        {
            throw new System.NotImplementedException();
        }


        public void GenerateMessage(string recievedMessage)
        {
            int recievedId = -1;
            doc = new XmlDocument();
            doc.LoadXml(recievedMessage); //loads the Data into the doc
            XmlNode node = doc.SelectSingleNode("//id"); //selects the id node

            if (node != null)
            {
                if (node.InnerText == "") //checks if the client has an id or is a new client
                {
                    int id = FindUsableID();
                    if (id != -1)
                    {
                        node.InnerText = id.ToString();
                        recievedId = id;
                        message = doc.OuterXml;
                        clients[id] = new Client { id = id }; //figure out which Id should be sent to the Client and add the client to the server array
                    }
                    else
                    {
                        message = "NO FREE ID";
                    }
                    
                }
                else
                {
                    recievedId = Convert.ToInt32(node.InnerText); //maybe needed for reconnect
                }
                //message = doc.OuterXml;
            }

            else //handle an wrong message
            {
                message = "Invalid Message Format";
            }

            try
            {

            
           
            if (clients[recievedId].word == null)
            {

                //Appends the word to the xml
                XmlNode hangManNode = doc.SelectSingleNode("//HangMan"); //selects the HangMan node

                if (hangManNode != null)
                {
                 XmlNode wordNode = doc.CreateElement("word");
                 string helper = GameLogic.Instance.randomWord(); //get the random word
                 clients[recievedId].word = helper;
                 wordNode.InnerText = GameLogic.Instance.hideWord(helper); //censor the random word
                 hangManNode.AppendChild(wordNode); //appends the word
                 message = doc.OuterXml;
                }

                else //handle an wrong message
                {
                  message = "Invalid Message Format";
                }

                }
            }

             catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            XmlNode letterNode = doc.SelectSingleNode("//letter"); //selects the letter node
            if (letterNode.InnerText != "")
            {
                char letter = letterNode.InnerText[0];
                int[] positions = GameLogic.Instance.findLetterInWord(clients[recievedId].word, letter);

                XmlNode wordNode = doc.SelectSingleNode("//word");
                string word = wordNode.InnerText;

                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < positions.Length; i++) //Replace the _ with the correct letter
                {
                    for (int j = 0; j < word.Length; j++)
                    {
                        if (positions[i] == j)
                        {
                            sb.Append(letter);
                        }
                        else
                        {
                            sb.Append(word[j]);
                        }
                        
                    }
                    word = sb.ToString();
                    sb.Clear();
                }

                if (!word.Contains("_"))
                {
                    RemoveClient(recievedId);
                }

                wordNode.InnerText = word;
                message = doc.OuterXml;
            }
        }

        private int FindUsableID() // if this returns a -1 -> No id found 
        {
            for (int i = 0; i < clients.Length; i++)
            {
                if (clients[i] == null)
                {
                    return i;
                }
            }
            return -1;
        }

    }
}