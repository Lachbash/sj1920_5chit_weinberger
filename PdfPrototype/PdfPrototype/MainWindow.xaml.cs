﻿using Syncfusion.Pdf;
using Syncfusion.Pdf.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Syncfusion.Pdf.Parsing;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Grid;
using Syncfusion.Pdf.Interactive;
using System.IO;

namespace PdfPrototype
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PdfDocument doc = new PdfDocument();

            //Creating section1 and adding pages to them
            PdfSection section1 = doc.Sections.Add();
            section1.Pages.Add();
            section1.Pages.Add();

            //Creating section2 and adding pages to them
            PdfSection section2 = doc.Sections.Add();
            section2.Pages.Add();
            section2.Pages.Add();

            RectangleF bounds = new RectangleF(0, 0, doc.Pages[0].GetClientSize().Width, 50);

            PdfFont font2 = new PdfStandardFont(PdfFontFamily.ZapfDingbats, 12f);

            PdfPageTemplateElement header = new PdfPageTemplateElement(bounds);

            PdfImage image = new PdfBitmap("logo.png");

            //Draw the image in the header.

            header.Graphics.DrawImage(image, new PointF(0, 0), new SizeF(100, 50));
            
            header.Graphics.DrawString("Hello World!!!", font2, PdfBrushes.Black, new PointF(200, 100));

            //-----

            PdfPageTemplateElement header2 = new PdfPageTemplateElement(bounds);

            PdfImage image2 = new PdfBitmap("logo.png");

            //Draw the image in the header.

            header2.Graphics.DrawImage(image2, new PointF(0, 0), new SizeF(100, 50));

            header2.Graphics.DrawString("Hello World!!!", font2, PdfBrushes.Black, new PointF(0, 100));


            PdfPageTemplateElement footer = new PdfPageTemplateElement(bounds);

            PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 7);

            PdfBrush brush = new PdfSolidBrush(System.Drawing.Color.Black);

            //Create page number field.

            PdfPageNumberField pageNumber = new PdfPageNumberField(font, brush);

            //Create page count field.

            PdfPageCountField count = new PdfPageCountField(font, brush);

            //Add the fields in composite fields.

            PdfCompositeField compositeField = new PdfCompositeField(font, brush, "Page {0} of {1}", pageNumber, count);

            compositeField.Bounds = footer.Bounds;

            //Draw the composite field in footer.

            compositeField.Draw(footer.Graphics, new PointF(470, 40));


            //Including header and footer to the 1st section
            section1.Template.Top = header;
            section1.Template.Bottom = footer;

            //Including header and footer to the 2nd section
            section2.Template.Top = header2;
            //section2.Template.Bottom = footer;

            doc.Save("PDF.pdf");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //Create a new PDF document.

            PdfDocument pdfDocument = new PdfDocument();

            //Add a page to the PDF document

            PdfPage pdfPage = pdfDocument.Pages.Add();

            //Create a header and draw the image.

            RectangleF bounds = new RectangleF(0, 0, pdfDocument.Pages[0].GetClientSize().Width, 50);

            PdfPageTemplateElement header = new PdfPageTemplateElement(bounds);

            PdfImage image = new PdfBitmap("logo.png");

            //Draw the image in the header.

            header.Graphics.DrawImage(image, new PointF(0, 0), new SizeF(100, 50));

            //Add the header at the top.

            pdfDocument.Template.Top = header;

            PdfLightTable pdfLightTable = new PdfLightTable();

            

            DataTable table = new DataTable();

            //Include columns to the DataTable.

            table.Columns.Add("Name");

            table.Columns.Add("Age");

            table.Columns.Add("Sex");

            //Include rows to the DataTable.

            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });
            table.Rows.Add(new string[] { "abc", "21", "Male" });


            //Assign data source.

            pdfLightTable.DataSource = table;

            //Draw PdfLightTable.

            pdfLightTable.Draw(pdfPage, new PointF(0, 0));

            //Create the Text Web Link.

            PdfTextWebLink textLink = new PdfTextWebLink();

            //Set the hyperlink

            textLink.Url = "http://www.syncfusion.com";

            //Set the link text

            textLink.Text = "Syncfusion .NET components and controls";

            PdfFont font2 = new PdfStandardFont(PdfFontFamily.Helvetica, 12f);

            //Set the font

            textLink.Font = font2;

            //Draw the hyperlink in PDF page

            textLink.DrawTextWebLink(pdfPage, new PointF(0, 500));

            //Create PDF graphics for the page.

            PdfGraphics graphics = pdfPage.Graphics;

            graphics.DrawString("Hello World!!!", font2, PdfBrushes.Black, new PointF(0, 600));
            graphics.DrawString("HHHHHllo World!!!", font2, PdfBrushes.Black, new PointF(0, 612)); 
            graphics.DrawString("Hello World!!!", font2, PdfBrushes.Black, new PointF(0, 624));

            //Create a Page template that can be used as footer.

            PdfPageTemplateElement footer = new PdfPageTemplateElement(bounds);

            PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 7);

            PdfBrush brush = new PdfSolidBrush(System.Drawing.Color.Black);

            //Create page number field.

            PdfPageNumberField pageNumber = new PdfPageNumberField(font, brush);

            //Create page count field.

            PdfPageCountField count = new PdfPageCountField(font, brush);

            //Add the fields in composite fields.

            PdfCompositeField compositeField = new PdfCompositeField(font, brush, "Page {0} of {1}", pageNumber, count);

            compositeField.Bounds = footer.Bounds;

            //Draw the composite field in footer.

            compositeField.Draw(footer.Graphics, new PointF(470, 40));

            //Add the footer template at the bottom.

            pdfDocument.Template.Bottom = footer;

            //Save and close the document.

            PdfPage pdfPage2 = pdfDocument.Pages.Add();

            pdfDocument.Save("Output2.pdf");

            pdfDocument.Close(true);
        
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //Create PDF document.

            PdfDocument document = new PdfDocument();

            PdfPage page = document.Pages.Add();

            //Add the first layer.

            PdfPageLayer layer = page.Layers.Add("Layer1");

            PdfGraphics graphics = layer.Graphics;

            graphics.TranslateTransform(100, 60);

            //Draw arc.

            PdfPen pen = new PdfPen(System.Drawing.Color.Red, 50);

            RectangleF bounds = new RectangleF(0, 0, 50, 50);

            graphics.DrawArc(pen, bounds, 360, 360);

            //Add another layer on the page.

            PdfPageLayer layer2 = page.Layers.Add("Layer2");

            graphics = layer2.Graphics;

            graphics.TranslateTransform(100, 180);

            //Draw ellipse.

            graphics.DrawEllipse(pen, bounds);

            //Save the document.

            document.Save("Sample.pdf");

            //Close the document

            document.Close(true);
        }
    }
}
