﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convolution_Operation
{
    public class S_Matrix
    {
        public int[,] input { get; set; }


        private int[,] Calculation ( int [,] kernel)
        {
            int[,] ergMatrix = new int[input.GetLength(0) - kernel.GetLength(0) + 1, input.GetLength(1)-kernel.GetLength(1)+1 ];

            
            for (int i = 0; i < input.GetLength(0) - kernel.GetLength(0) + 1; i++)
            {
                for (int t = 0; t < input.GetLength(1)-kernel.GetLength(1) +1; t++)
                {
                    for (int j = 0; j < kernel.GetLength(0); j++)
                    {
                        for (int k = 0; k < kernel.GetLength(1); k++)
                        {
                            ergMatrix[i,t] += kernel[j, k] * input[i + j, k +t];
                            
                        }
                    }
                }

            }

            return ergMatrix;
        }

        public static S_Matrix operator * (S_Matrix sm1, S_Matrix sm2)
        {
            S_Matrix s_Matrix = new S_Matrix(){ input = sm1.Calculation(sm2.input) };
            return s_Matrix;
        }
    }
}
