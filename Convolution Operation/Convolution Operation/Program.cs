﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convolution_Operation
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Diagnostics.Stopwatch s = new System.Diagnostics.Stopwatch();

            S_Matrix sm1 = new S_Matrix() {input = new int[,]{{ 1, 2, 3, 4 },
                { 5, 6, 7, 8 },
                { 9, 10, 11, 12 } }
            };

            S_Matrix sm2 = new S_Matrix()
            {
                input = new int[,] {
                {1,2 },
                {3,4}
            }
            };

            RGBMatrix rgbm1 = new RGBMatrix()
            {
                input = new int[,,]
            {
                { {1,2,3},{2,3,4},{3,4,5} },
                { {4,5,6},{5,6,7},{6,7,8} },
                { {1,2,3},{2,3,4},{3,4,5} }
            }
            };

            RGBMatrix rgbm2 = new RGBMatrix()
            {
                input = new int[,,]
           {
                { {1,1},{1,1} },
                { {2,2},{2,2} },
                { {3,3},{3,3} }
           }
            };

            S_Matrix s_Matrix = new S_Matrix() { input = new int[7500, 10] };
            S_Matrix _Matrix = new S_Matrix()
            {
                input = new int[,]
                {
                    {2,3 },
                    {1,4 }
                }
            };
            s.Start();
            S_Matrix matrixErg = s_Matrix * _Matrix;
            s.Stop();
            Console.WriteLine(s.Elapsed);


            Random rand = new Random();
            for (int i = 0; i < s_Matrix.input.GetLength(0); i++)
            {
                for (int j = 0; j < s_Matrix.input.GetLength(1); j++)
                {
                    s_Matrix.input[i, j] = rand.Next(1, 5);
                }
            }
            s.Reset();
            s.Start();
            S_Matrix smErg = sm1 * sm2;
            s.Stop();
            Console.WriteLine(s.Elapsed);

            S_Matrix smErg_RGB = rgbm1 * sm2;
            S_Matrix smErg_RGB1 = rgbm2 * sm2;

            //showMatrix(sm1.input);
            //Console.WriteLine();
            //showMatrix(sm2.input);
            //Console.WriteLine();
            //Console.WriteLine();
            //showMatrix(smErg_RGB.input);
            //Console.WriteLine();
            //showMatrix(smErg_RGB1.input);
            Console.WriteLine();
            Console.ReadLine();
        }

        private static void showMatrix(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }

       
    }
}
