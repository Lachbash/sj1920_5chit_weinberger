﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Convolution_Operation;


namespace ConvolutionOperationTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ConvolutionTest()
        {
            S_Matrix sm = new S_Matrix()
            {
                input = new int[,] {
                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 5, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 2, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 5 }
            }
            };
            S_Matrix sm2 = new S_Matrix()
            {
                input = new int[,]
                {
                    {1,2,3},
                    {1,2,3},
                    {1,2,3}
                }
            };
            S_Matrix smErg = sm * sm2;
            int[,] checkarray = new int[,]
            {
            {18, 18, 18, 18, 18, 18, 18, 18},
            {18, 18, 30, 26, 22, 18, 18, 18},
            {18, 18, 33, 28, 23, 18, 18, 18},
            {18, 18, 33, 28, 23, 18, 18, 18},
            {18, 18, 21, 20, 19, 18, 18, 18},
            {18, 18, 18, 18, 18, 18, 18, 30}
            };
            CollectionAssert.AreEqual(smErg.input, checkarray);
           
        }
    }
}
